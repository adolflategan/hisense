<?php
    /*
     No Additional Setting Options
    */
    if (!class_exists('TS_Parameter_GoPricing')) {
        class TS_Parameter_GoPricing {
            function __construct() {	
                if (function_exists('add_shortcode_param')) {
                    add_shortcode_param('gopricing', array(&$this, 'gopricing_settings_field'));
                }
            }        
            function gopricing_settings_field($settings, $value) {
                global $VISUAL_COMPOSER_EXTENSIONS;
                $dependency     = vc_generate_dependencies_attributes($settings);
                $param_name     = isset($settings['param_name']) ? $settings['param_name'] : '';
                $type           = isset($settings['type']) ? $settings['type'] : '';
                $radios         = isset($settings['options']) ? $settings['options'] : '';
                $url            = $VISUAL_COMPOSER_EXTENSIONS->TS_VCSC_PluginPath;
                $output         = '';
                $pricing_tables = get_option('go_pricing_tables');
                if (!empty($pricing_tables)) {
                    $output .= '<select name="' . $param_name . '" id="' . $param_name . '" class="ts-go-pricing-tables wpb-input wpb-select dropdown wpb_vc_param_value ' . $param_name . ' ' . $type . '" style="margin-bottom: 20px;">';
                        foreach ($pricing_tables as $pricing_table) {
                            $tableID 	= $pricing_table['table-id'];
                            $tableName	= $pricing_table['table-name'];
                            if ($value == $tableID) {
                                $output 	.= '<option class="" value="' . $tableID . '" selected>' . $tableName . '</option>';
                            } else {
                                $output 	.= '<option class="" value="' . $tableID . '">' . $tableName . '</option>';
                            }
                        }
                    $output .= '</select>';
                } else {
                    $output .= '<select name="' . $param_name . '" id="' . $param_name . '" class="ts-go-pricing-tables wpb-input wpb-select dropdown wpb_vc_param_value ' . $param_name . ' ' . $type . '" style="margin-bottom: 20px;">';
                        $output 	.= '<option class="" value="None">No Tables could be found!</option>';
                    $output .= '</select>';
                }
                return $output;
            }
        }
    }
    if (class_exists('TS_Parameter_GoPricing')) {
        $TS_Parameter_GoPricing = new TS_Parameter_GoPricing();
    }
?>