<?php
    /*
     No Additional Setting Options
    */
    if (!class_exists('TS_Parameter_QuForm')) {
        class TS_Parameter_QuForm {
            function __construct() {	
                if (function_exists('add_shortcode_param')) {
                    add_shortcode_param('quform', array(&$this, 'quform_settings_field'));
                }
            }        
            function quform_settings_field($settings, $value) {
                global $VISUAL_COMPOSER_EXTENSIONS;
                $dependency     = vc_generate_dependencies_attributes($settings);
                $param_name     = isset($settings['param_name']) ? $settings['param_name'] : '';
                $type           = isset($settings['type']) ? $settings['type'] : '';
                $radios         = isset($settings['options']) ? $settings['options'] : '';
                $url            = $VISUAL_COMPOSER_EXTENSIONS->TS_VCSC_PluginPath;
                $output         = '';
                if (function_exists('iphorm_get_all_forms')) {
                    $quforms_forms 	= iphorm_get_all_forms();
                    if (count($quforms_forms)) {
                        $output .= '<select name="' . $param_name . '" id="' . $param_name . '" class="ts-quform-selector wpb-input wpb-select dropdown wpb_vc_param_value ' . $param_name . ' ' . $type . '" style="margin-bottom: 20px;">';
                        foreach ($quforms_forms as $form) {
                            $formID 	= $form['id'];
                            $formName	= $form['name'];
                            $formStatus	= $form['active'];
                            if ($formStatus == 0) {
                                if ($value == $formID) {
                                    $output .= '<option data-name="' . $formName . '" class="" value="' . $formID . '" selected>' . $formName . ' (inactive)</option>';
                                } else {
                                    $output .= '<option data-name="' . $formName . '" class="" value="' . $formID . '">' . $formName . ' (inactive)</option>';
                                }
                            } else {
                                if ($value == $formID) {
                                    $output .= '<option data-name="' . $formName . '" class="" value="' . $formID . '" selected>' . $formName . '</option>';
                                } else {
                                    $output .= '<option data-name="' . $formName . '" class="" value="' . $formID . '">' . $formName . '</option>';
                                }
                            }
                        }
                        $output .= '</select>';
                    } else {
                        printf(esc_html__('No forms found, %sclick here to create one%s.', 'ts_visual_composer_extend'), '<a href="' . admin_url('admin.php?page=iphorm_form_builder') . '">', '</a>');
                    }
                }
                return $output;
            }
        }
    }
    if (class_exists('TS_Parameter_QuForm')) {
        $TS_Parameter_QuForm = new TS_Parameter_QuForm();
    }
?>