<?php
/*
Plugin Name: Equal Height Columns for Visual Composer
Plugin URI: http://creativebackpack.com
Description: Extend Visual Composer with your own set of shortcodes.
Version: 1.0.0
Author: CreativeBackpack
Author URI: http://creativebackpack.com
License: GPLv2 or later
*/

if (!defined('ABSPATH')) die('-1');

class VCExtendAddonClass {
	
	private $ehc_config = array();
	
    function __construct() {
        // We safely integrate with VC with this hook
		
        add_action( 'init', array( $this, 'integrateWithVC' ) );

		add_filter( 'vc_shortcodes_css_class', array( $this, 'custom_css_classes_for_vc_row_and_vc_column'), 10, 2 );

        // Register CSS and JS
        add_action( 'wp_enqueue_scripts', array( $this, 'loadCssAndJs' ) );
		add_action( 'wp_print_footer_scripts', array ( $this, 'ehc_config' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'loadAdminCssAndJs' ) );
    }
 
    public function integrateWithVC() {
        // Check if Visual Composer is installed
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            // Display notice that Visual Compser is required
            add_action('admin_notices', array( $this, 'showVcVersionNotice' ));
            return;
        }
 
		$rowAttributes = array(
			'type' => 'dropdown',
			'heading' => 'Equal Height Columns',
			'param_name' => 'equal_height_columns',
			'value' => array( "no", "yes" ),
			'admin_enqueue_js' => array( plugins_url('assets/scripts-admin.min.js',__FILE__)),
			'admin_enqueue_css' => array(plugins_url('assets/styles-admin.min.css', __FILE__)), 
			'dependency' => array(
				'element' => 'el_class',
				'not_empty' => true,
				'callback' => 'vcRowDependencyCallback',
			),
		);
		
		$columnAttributes = array(
			'type' => 'dropdown',
			'heading' => 'Background Clip Content',
			'param_name' => 'background_clip_content',
			'value' => array( "no", "yes" ),
			'admin_enqueue_js' => array( plugins_url('assets/scripts-admin.min.js',__FILE__)),
			'admin_enqueue_css' => array(plugins_url('assets/styles-admin.min.css', __FILE__)), 
			'dependency' => array(
				'element' => 'el_class',
				'not_empty' => true,
				'callback' => 'vcColumnDependencyCallback',
			),
		);
		
		$contentVerticalAlignAttributes = array(
			'type' => 'dropdown',
			'heading' => 'Content vertical align',
			'param_name' => 'content_vertical_align',
			'value' => array( "no", "yes" ),
			'admin_enqueue_js' => array( plugins_url('assets/scripts-admin.min.js',__FILE__)),
			'admin_enqueue_css' => array(plugins_url('assets/styles-admin.min.css', __FILE__)), 
			'dependency' => array(
				'element' => 'el_class',
				'not_empty' => true,
				'callback' => 'vcContentVerticalAlignColumnDependencyCallback',
			),
		);
		
		vc_add_param( 'vc_row', $rowAttributes ); 
		vc_add_param( 'vc_row_inner', $rowAttributes ); 
		
		vc_add_param( 'vc_column', $columnAttributes ); 
		vc_add_param( 'vc_column_inner', $columnAttributes ); 
			
		vc_add_param( 'vc_column', $contentVerticalAlignAttributes ); 
		vc_add_param( 'vc_column_inner', $contentVerticalAlignAttributes ); 

	}

    /*
    Load plugin css and javascript files which you may need on front end of your site
    */
    public function loadCssAndJs() {
      wp_register_style( 'vc_equal_height_columns_style', plugins_url('assets/styles.min.css', __FILE__) );
      wp_enqueue_style( 'vc_equal_height_columns_style' );

      wp_enqueue_script( 'jquerymatchHeight-min.js', plugins_url('assets/jquery.matchHeight-min.js', __FILE__), array('jquery') );
      wp_enqueue_script( 'vc_equal_height_columns_js', plugins_url('assets/scripts.min.js', __FILE__), array('jquery') );
    }
	
    public function loadAdminCssAndJs() {
      wp_register_style( 'vc_equal_height_columns_admin_style', plugins_url('assets/styles-admin.min.css', __FILE__) );
      wp_enqueue_style( 'vc_equal_height_columns_admin_style' );		
		
      wp_enqueue_script( 'vc_equal_height_columns_admin_scripts_js', plugins_url('assets/scripts-admin.min.js', __FILE__), array('jquery') );
    }	

	public function ehc_config() {
		echo "<script>window.ehc_config = " . json_encode($this->ehc_config) . ";</script>";
	}

	public function custom_css_classes_for_vc_row_and_vc_column( $class_string, $tag ) {
	  if ( $tag == 'vc_row' || $tag == 'vc_row_inner' ) {
		  $classes = explode(" ", $class_string);
		  $row_class = $classes[1];
		  $this->ehc_config['row_class'] = $row_class;
	  }

	  if ( $tag == 'vc_column' || $tag == 'vc_column_inner' ) {
		  $classes = explode(" ", $class_string);
		  $column_class = $classes[1];
		  $this->ehc_config['column_class'] = $column_class;
	  }
		
	  return $class_string; // Important: you should always return modified or original $class_string
	}

    /*
    Show notice if your plugin is activated but Visual Composer is not
    */
    public function showVcVersionNotice() {
        $plugin_data = get_plugin_data(__FILE__);
        echo '
        <div class="updated">
          <p>'.sprintf(__('<strong>%s</strong> requires <strong><a href="http://bit.ly/vcomposer" target="_blank">Visual Composer</a></strong> plugin to be installed and activated on your site.', 'vc_extend'), $plugin_data['Name']).'</p>
        </div>';
    }
}

new VCExtendAddonClass();