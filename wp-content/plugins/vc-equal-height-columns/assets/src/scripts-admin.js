var vcRowDependencyCallback = function () {
	var $ = jQuery;
	var ehc_class = 'equal-height-columns';
	var ehc_dropdown = this.$content.find('[name="equal_height_columns"]');
	var ehc_dropdown_val = ehc_dropdown.val();
	var extra_class_input = this.$content.find('[data-param_name="el_class"]').find('input');
	ehc_dropdown.on('change', function() {
		var new_value = jQuery(this).val();
		var value_changed = new_value !== ehc_dropdown_val;
		var extra_class_input_val = extra_class_input.val();
		var has_already_class = extra_class_input_val.indexOf(ehc_class) > -1
		if (value_changed) {
			ehc_dropdown_val = new_value
			if (new_value == "yes") {
				if (!has_already_class) {
					extra_class_input.val(extra_class_input_val + " " + ehc_class);
				}
			} else {
				extra_class_input.val(extra_class_input_val.replace(ehc_class, ""));
			}
		}
	});
};

var vcColumnDependencyCallback = function () {
	var $ = jQuery;
	var ehc_class = 'background-clip-content';
	var ehc_dropdown = this.$content.find('[name="background_clip_content"]');
	var ehc_dropdown_val = ehc_dropdown.val();
	var extra_class_input = this.$content.find('[data-param_name="el_class"]').find('input');
	ehc_dropdown.on('change', function() {
		var new_value = jQuery(this).val();
		var value_changed = new_value !== ehc_dropdown_val;
		var extra_class_input_val = extra_class_input.val();
		var has_already_class = extra_class_input_val.indexOf(ehc_class) > -1
		if (value_changed) {
			ehc_dropdown_val = new_value
			if (new_value == "yes") {
				if (!has_already_class) {
					extra_class_input.val(extra_class_input_val + " " + ehc_class);
				}
			} else {
				extra_class_input.val(extra_class_input_val.replace(ehc_class, ""));
			}
		}
	});
};

var vcContentVerticalAlignColumnDependencyCallback = function () {
	var $ = jQuery;
	var ehc_class = 'content-vertical-align';
	var ehc_dropdown = this.$content.find('[name="content_vertical_align"]');
	var ehc_dropdown_val = ehc_dropdown.val();
	var extra_class_input = this.$content.find('[data-param_name="el_class"]').find('input');
	ehc_dropdown.on('change', function() {
		var new_value = jQuery(this).val();
		var value_changed = new_value !== ehc_dropdown_val;
		var extra_class_input_val = extra_class_input.val();
		var has_already_class = extra_class_input_val.indexOf(ehc_class) > -1
		if (value_changed) {
			ehc_dropdown_val = new_value
			if (new_value == "yes") {
				if (!has_already_class) {
					extra_class_input.val(extra_class_input_val + " " + ehc_class);
				}
			} else {
				extra_class_input.val(extra_class_input_val.replace(ehc_class, ""));
			}
		}
	});
};
