jQuery(window).load(function() {
	jQuery('[data-equal-height-columns="yes"], .equal-height-columns').each(function(){
		var columns_class = ".wpb_column";
		var first_column = jQuery(this).find(columns_class).first();
		var columns_container = first_column.parent();
		var columns = columns_container.children(columns_class);
		columns.matchHeight();
	});		
});