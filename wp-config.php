<?php
define('WP_CACHE', TRUE);
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hisense_2018');

/** MySQL database username */
define('DB_USER', 'hisensec');

/** MySQL database password */
define('DB_PASSWORD', 'M3vV,da(biio');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'spoh75dut9e0t0bbymowwhcoqip2xb6vr04eka1bhz9uabbempob6wulhfemilfj');
define('SECURE_AUTH_KEY',  '36g2yycvxwawue8wapxyttzkkloqbi26tjorr5dzed0opgyfdu9ifum3w16evwot');
define('LOGGED_IN_KEY',    's5djc9nafxkh1n7riqqtlbgbazn6k5wkto0l14nbuhzlllezfrudrgmty4rkawav');
define('NONCE_KEY',        'gfosq2w5xryfdozjatgzchayfslzkun4mwe9skipzdifakkbf9cxmj5wm1ianr7p');
define('AUTH_SALT',        '65dqut1awqelakcyshswl1dqqbq2vkn1lhcxtabnn75mpekqdeqmger7v65dspxm');
define('SECURE_AUTH_SALT', 'fqgh7qke2wnyvsqm4nq68uwxl6iwnb7jj6oqyhmalkr3vsrh0edcc6wblrirnknf');
define('LOGGED_IN_SALT',   'smfr662tmy9lf0xjenaneirq2jjfovtehgoce23ks15xy7pwcsklp6fvhfolg8bq');
define('NONCE_SALT',       '7kk51rvculotrldchtj1rtustspypzxq8dino49bsh3vgmxdpirgsmcumnl45jha');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

define( 'WP_MEMORY_LIMIT', '256M' );

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);
define('FORCE_SSL_ADMIN', true);



/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


define('SNAPSHOT_FILESET_CHUNK_SIZE', 10);
define('SNAPSHOT_TABLESET_CHUNK_SIZE', 100);
define('SNAPSHOT_FILESET_LARGE_FILE_SIZE', 104857600); /**skip files larger than ~100MB**/
if (!defined('SNAPSHOT_FORCE_ZIP_LIBRARY')) define('SNAPSHOT_FORCE_ZIP_LIBRARY', 'pclzip'); //use PclZip library
if (!defined('SNAPSHOT_IGNORE_SYMLINKS')) define('SNAPSHOT_IGNORE_SYMLINKS', true); //skip symbolic links
